// ext libs
const express = require('express');
const httpContext = require('express-http-context');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const mongoose = require('mongoose');

// routes
const routes = require('./routes/');

// services
const logger = require('./system/logger.service');

const PORT = 4000;

// init express
const app = express();

/**
 * MIDDLEWARE
 */

app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(httpContext.middleware);

/**
 * ROUTER
 */
app.use('/v1', routes);

/**
 * ERROR HANDLER
 */

app.use((error, request, response, next) => {
    logger.error('[main] error is - ', error);
    response.status(500).json({
        error: true,
        code: error.name,
        message: error.message
    });
});

/**
 * SERVER
 */
mongoose.connect(
    'mongodb+srv://admin:pM80jJlQYscB19TA@get-hyphen-l9cym.mongodb.net/test?retryWrites=true&w=majority',
    { useNewUrlParser: true, dbName: 'general' }
).then(() => {
    app.listen(
        PORT,
        () => logger.info(`Server started on ${PORT}`)
    );
});
