// ext libs
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const voteSchema = new Schema({
    userId: String,
    Vote: Number,
    questionId: String
});

const VoteModel = mongoose.model('votes', voteSchema);

module.exports = VoteModel;
