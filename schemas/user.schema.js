// ext libs
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    User: String,
    department: String,
    designation: String,
    location: String
});

const UserModel = mongoose.model('users', userSchema);

module.exports = UserModel;
