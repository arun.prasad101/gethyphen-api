// ext libs
const VError = require('verror');

// system services
const logger = require('../system/logger.service');

// services
const userService = require('./users.service');
const votesService = require('./votes.service');

// utils
const _get = require('lodash/get');

// configuration

/**
 * Method to format the data to be returned.
 *
 * Creates objects containing the value of the dimension
 * selected along with sentiment and participation %.
 *
 * @param votesByDimension
 * @param usersByDimension
 * @returns {Array}
 */
function formatData({ votesByDimension, usersByDimension }) {
    const formattedData = [];

    usersByDimension.forEach((current, index) => {

        const { participationPercentage, votes } = votesByDimension[index];

        const positiveVotes = _get(votes[2], 'count', 0);
        const negativeVotes = _get(votes[0], 'count', 0);

        formattedData.push({
            dimension: current._id,
            participationPercentage: participationPercentage,
            sentiment: positiveVotes - negativeVotes
        });

    });

    return formattedData;
}

/**
 * Method to get sentiments by the dimension passed.
 *
 * First the userIds are retrieved by dimension provided
 * and for each set of userIds the corresponding votes are
 * obtained.
 *
 * Based on the votes and userIds, the data is formatted. For each set of
 * votes the sentiment is calculated and saved along with the dimension value.
 *
 * Method also calculates the total sentiment.
 *
 * @param dimension
 * @returns {Promise<{totalSentiment: *, sentimentByDimension: Array}>}
 */
exports.getSentimentByDimension = async function ({ dimension }) {
    try {
        // get users by dimension
        const usersByDimension =
            await userService.getUserByDimension({ dimension });

        // get votes by user Ids
        const pendingOps = [];
        usersByDimension.forEach(current =>
            pendingOps.push(votesService.getVotesByUserIds(current.users))
        );
        const votesByDimension = await Promise.all(pendingOps);

        // format data for return i.e sentiment & participation % by dimension
        const sentimentByDimension = formatData({ votesByDimension, usersByDimension });

        // calculate the total sentiment
        const totalSentiment = sentimentByDimension.reduce(
            (acc, current) => current.sentiment + acc,
            0
        );

        return { totalSentiment, sentimentByDimension };
    } catch (e) {
        logger.error('[SentimentService::getSentimentByDimension] error - ' + e);
        throw new VError(
            { cause: e, name: 'SentimentInternalError' },
            'An error occurred while getting sentiment by %s', dimension
        )
    }
};
