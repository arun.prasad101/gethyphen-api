// ext libs
const VError = require('verror');
// services
const logger = require('../system/logger.service');

// data access
const userDataAccess = require('../dataAccess/user.dataAccess');

// utils
// configuration

/**
 *
 * @param dimension
 * @returns {Promise<Array>}
 */
exports.getUserByDimension = async function getUserByDimension({ dimension }) {
    try {
        logger.info('[UserService::getUserByDimension] begin - ' + dimension);
        return await userDataAccess.getUsersByDimension({ dimension });
    } catch (e) {
        logger.error('[UserService::getUserByDimension] error - ' + e);
        throw new VError(
            {cause: e, name: 'UserInternalError'},
            'Error getting users by %s', dimension
        );
    }
};
