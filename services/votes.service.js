// ext libs
const VError = require('verror');

// services
const logger = require('../system/logger.service');

// dataAccess
const VoteDataAccess = require('../dataAccess/vote.dataAccess');

// utils

function getTotalNumberOfVoters(data) {
    return data.reduce((acc, current) => acc + current.count, 0) / 3
}

exports.getVotesByUserIds = async function getVotesByUserIds(userIds) {
    try {
        logger.info('[VoteService::getVotesByUserIds] begin ' + userIds);

        // get votes by user ids
        const data = await VoteDataAccess.getVotesByUserIds({ userIds });

        // calculate the participation % for the current set of users
        const numberOfVoters = getTotalNumberOfVoters(data);
        const participationPercentage = (numberOfVoters / userIds.length) * 100;

        return { votes: data, participationPercentage };
    } catch (e) {
        logger.error('[VoteService::getVotesByUserIds] error - ' + e);
        throw new VError(
            { cause: e, name: 'VotesInternalError' },
            'Error getting votes by user ids'
        );
    }
};
