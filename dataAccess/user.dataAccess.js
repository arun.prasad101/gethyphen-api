// ext libs
// system services
// models
const UserModel = require('../schemas/user.schema');

// utils
// configuration

/**
 * Method to get a list of user ids grouped by the dimension provided.
 *
 * @param dimension {String} possible values - [ 'location', 'designation', 'department' ]
 * @returns {Promise<Array>}
 */
exports.getUsersByDimension = async function getUserByDimension({ dimension }) {
    return await UserModel.aggregate([
        { $group: {
            _id: `$${dimension}`,
            users: { $push: '$User' }
        }}
    ]).exec();
};
