// ext libs
// system services
// models
const VoteModel = require('../schemas/votes.schema');

// utils
// configuration

/**
 * Configuration for buckets to be created by the
 * mongo DB aggregation pipeline.
 */
const boundaries = [
    Number(1),
    Number(3),
    Number(4),
    Number(6)
];

/**
 * Method to get the votes cast by the userIds passed.
 *
 * The method will group the votes by positive, neutral and negative
 * sentiments, also providing the count of votes for each sentiment.
 *
 * @param userIds {String[]} array of user ids
 * @returns {Promise<Array>}
 */
exports.getVotesByUserIds = async function({ userIds }) {
    return await VoteModel.aggregate([
        { $match: { userId: { $in: userIds } } },
        {
            $bucket: {
                groupBy: '$Vote',
                boundaries,
                output: {
                    "count": { $sum: 1 },
                    "votes": { $push: { "vote": "$Vote" } }
                }
            }
        }
    ]).exec();
};
