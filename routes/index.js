const express = require('express');
const router = express.Router();

const sentimentRoutes = require('./sentiment.route');
router.use('/sentiment', sentimentRoutes);

module.exports = router;
