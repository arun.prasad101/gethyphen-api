// ext libs
const express = require('express');
const router = express.Router();

// system services
const logger = require('../system/logger.service');

// service
const sentimentService = require('../services/sentiment.service');

// utils
// configuration

/**
 * Router handler to get sentiments by dimension.
 */
router.get('/:dimension',
    async (request, response, next) => {

    try {
        logger.info('[SentimentRoute::getSentimentByDimension] begin');
        const { dimension } = request.params;

        const result =
            await sentimentService.getSentimentByDimension({ dimension });

        response.status(200)
            .json({ error: false, dimension, ...result })
    } catch (e) {
        return next(e);
    }
});

module.exports = router;
