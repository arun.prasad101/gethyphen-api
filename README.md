# API

## Steps to run
1. Clone the repo
2. npm install
3. npm start

## Completed
- Integration of mongoose ODM
- API to return sentiments by selected segment

## Incomplete
- Test cases
- Authentication & validation
- Security

## Known Issues